import {createBrowserRouter} from "react-router-dom";
import HomeView from "../view/HomeView";
import React from "react";
import ItemsView from "../view/ItemsView";
import ItemView from "../view/ItemView";
import BasketView from "../view/BasketView";
import OrderView from "../view/OrderView";
import AdminView from "../view/AdminView";

const router = createBrowserRouter([
    {
        path: "/",
        element: <HomeView/>,
    },
    {
        path: "/items",
        element: <ItemsView/>,
    },
    {
        path: "/items/:itemId",
        element: <ItemView/>,
    },
    {
        path: "/basket",
        element: <BasketView/>,
    },
    {
        path: "/order",
        element: <OrderView/>
    },
    {
        path: "/admin",
        element: <AdminView/>
    }

], {basename: "/"});
export default router;