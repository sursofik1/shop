import React, {useState} from 'react';
import classes from "./AdminItem.module.css";
import {useDispatch} from "react-redux";
import {deleteItem, editItem} from "../../store/items.store";

const AdminItem = (props) => {
    const dispatch = useDispatch();
    const [isEdit, setEdit] = useState(false);
    const [item, setItem] = useState({...props.item});
    function change(key, value) {
        let changeItem = {...item}
        changeItem[key] = value;
        setItem(changeItem);
    }
    function submit(){
        dispatch(editItem(item));
        setEdit(false)
    }
    return (
        <div className={classes.itemBox}>
            <div className={classes.itemBox1}>
                <img src={props.item.url}/>
                {(!isEdit) ?
                    <div>
                        <p>id: {props.item.id}</p>
                        <p>name: {props.item.name}</p>
                        <p>description: {props.item.description}</p>
                        <p>size: {props.item.size}</p>
                        <p>size_type: {props.item.size_type}</p>
                        <p>barcode: {props.item.barcode}</p>
                        <p>producer: {props.item.producer}</p>
                        <p>brand: {props.item.brand}</p>
                        <p>type: {props.item.type}</p>
                        <p>price: {props.item.price}</p>
                        <div>
                            <button onClick={() => setEdit(true)}>Редактировать</button>
                            <button onClick={() => dispatch(deleteItem(props.item.id))}>Удалить</button>
                        </div>
                    </div>
                    :
                    <div>
                        <div>
                            <p>id: {item.id}</p>
                        </div>
                        <div>
                            <label>name:
                                <input value={item.name}
                                       onChange={(event)=>change('name',  event.target.value)}></input>
                            </label>
                        </div>
                        <div>
                            <label>description:
                                <input value={item.description}
                                       onChange={(event)=>change('description',  event.target.value)}></input>
                            </label>
                        </div>
                        <div>
                            <label>size:
                                <input value={item.size}
                                       onChange={(event)=>change('size',  event.target.value)}></input>
                            </label>
                        </div>
                        <div>
                            <label>size_type:
                                <input value={item.size_type}
                                       onChange={(event)=>change('size_type',  event.target.value)}></input>
                            </label>
                        </div>
                        <div>
                            <label>barcode:
                                <input value={item.barcode}
                                       onChange={(event)=>change('barcode',  event.target.value)}></input>
                            </label>
                        </div>
                        <div>
                            <label>producer:
                                <input value={item.producer}
                                       onChange={(event)=>change('producer',  event.target.value)}></input>
                            </label>
                        </div>
                        <div>
                            <label>brand:
                                <input value={item.brand}
                                       onChange={(event)=>change('brand',  event.target.value)}></input>
                            </label>
                        </div>
                        <div>
                            <label>type:
                                <input value={item.type}
                                       onChange={(event)=>change('type',  event.target.value)}></input>
                            </label>
                        </div>
                        <div>
                            <label>price:
                                <input value={item.price}
                                       onChange={(event)=>change('price',  event.target.value)}></input>
                            </label>
                        </div>
                        <button onClick={submit}>Сохранить</button>
                    </div>
                }
            </div>
        </div>
    );
};

export default AdminItem;