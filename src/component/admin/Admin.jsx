import React from 'react';
import classes from "./Admin.module.css";
import {useSelector} from "react-redux";
import AdminItem from "./AdminItem";
const Admin = () => {
    const items = useSelector(state => state.items.list);
    return (
        <div>
            <div className={classes.itemsBox}>
                {items.map(item =>
                    <AdminItem item={item} key={item.id}/>
                )}
            </div>
        </div>
    );
};

export default Admin;