import React from 'react';
import classes from "./Order.module.css";
const Order = () => {
    return (
        <div className={classes.order}>
            <h2>Спасибо за заказ!</h2>
        </div>
    );
};

export default Order;