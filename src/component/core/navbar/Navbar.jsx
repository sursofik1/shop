import React from 'react';
import classes from "./Navbar.module.css";

const Navbar = () => {
    return (
        <div>
            <div className={classes.navbar}>
                <div className={classes.navbarBox}>
                    <div className={classes.navbarBox1}>
                        <svg width="16" height="18" viewBox="0 0 16 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M8 9.8335C9.38071 9.8335 10.5 8.71421 10.5 7.3335C10.5 5.95278 9.38071 4.8335 8 4.8335C6.61929 4.8335 5.5 5.95278 5.5 7.3335C5.5 8.71421 6.61929 9.8335 8 9.8335Z"
                                stroke="#3F4E65" strokeWidth="1.3" strokeLinecap="round" strokeLinejoin="round"/>
                            <path
                                d="M7.99992 0.666748C6.23181 0.666748 4.53612 1.36913 3.28587 2.61937C2.03563 3.86961 1.33325 5.5653 1.33325 7.33342C1.33325 8.91008 1.66825 9.94175 2.58325 11.0834L7.99992 17.3334L13.4166 11.0834C14.3316 9.94175 14.6666 8.91008 14.6666 7.33342C14.6666 5.5653 13.9642 3.86961 12.714 2.61937C11.4637 1.36913 9.76803 0.666748 7.99992 0.666748V0.666748Z"
                                stroke="#3F4E65" strokeWidth="1.3" strokeLinecap="round" strokeLinejoin="round"/>
                        </svg>
                        <div>
                            <p className={classes.navbarText1}>г. Кокчетав, ул. Ж. Ташенова 129Б</p>
                            <p className={classes.navbarText2}>(Рынок Восточный)</p>
                        </div>
                    </div>
                    <div className={classes.navbarBox1}>
                        <svg width="2" height="39" viewBox="0 0 2 39" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path opacity="0.05" d="M1 0V39" stroke="black" strokeDasharray="2 2"/>
                        </svg>
                        <svg width="18" height="14" viewBox="0 0 18 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M3.37508 0.333252H14.6251C15.3169 0.333207 15.9824 0.59788 16.4853 1.07298C16.9881 1.54808 17.2901 2.19758 17.3293 2.88825L17.3334 3.04158V10.9583C17.3335 11.65 17.0688 12.3156 16.5937 12.8184C16.1186 13.3213 15.4691 13.6233 14.7784 13.6624L14.6251 13.6666H3.37508C2.6833 13.6666 2.01772 13.402 1.51489 12.9269C1.01205 12.4518 0.71008 11.8023 0.670915 11.1116L0.666748 10.9583V3.04158C0.666703 2.3498 0.931376 1.68423 1.40647 1.18139C1.88157 0.678558 2.53108 0.376584 3.22175 0.337419L3.37508 0.333252H14.6251H3.37508ZM16.0834 4.81075L9.29175 8.38575C9.21506 8.42626 9.13078 8.45037 9.04427 8.45654C8.95776 8.46271 8.87091 8.4508 8.78925 8.42159L8.70925 8.38658L1.91675 4.81158V10.9583C1.91676 11.3242 2.05439 11.6768 2.30231 11.9461C2.55024 12.2153 2.89033 12.3815 3.25508 12.4116L3.37508 12.4166H14.6251C14.9912 12.4166 15.3439 12.2788 15.6132 12.0307C15.8824 11.7826 16.0485 11.4423 16.0784 11.0774L16.0834 10.9583V4.81075ZM14.6251 1.58325H3.37508C3.00909 1.58327 2.65648 1.72089 2.38726 1.96882C2.11803 2.21674 1.95186 2.55683 1.92175 2.92159L1.91675 3.04158V3.39908L9.00008 7.12659L16.0834 3.39825V3.04158C16.0834 2.67546 15.9456 2.32274 15.6975 2.0535C15.4494 1.78425 15.1091 1.61817 14.7442 1.58825L14.6251 1.58325Z"
                                fill="#3F4E65"/>
                        </svg>
                        <div>
                            <p className={classes.navbarText1}>opt.sultan@mail.ru</p>
                            <p className={classes.navbarText2}>На связи в любое время</p>
                        </div>
                    </div>
                </div>
                <div className={`${classes.navbarBox} ${classes.navbarText3}`}>
                    <p>О компании</p>
                    <svg width="2" height="39" viewBox="0 0 2 39" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path opacity="0.05" d="M1 0V39" stroke="black" strokeDasharray="2 2"/>
                    </svg>
                    <p>Доставка и оплата</p>
                    <svg width="2" height="39" viewBox="0 0 2 39" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path opacity="0.05" d="M1 0V39" stroke="black" strokeDasharray="2 2"/>
                    </svg>
                    <p>Возврат</p>
                    <svg width="2" height="39" viewBox="0 0 2 39" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path opacity="0.05" d="M1 0V39" stroke="black" strokeDasharray="2 2"/>
                    </svg>
                    <p>Контакты</p>
                </div>
            </div>
            <hr className={classes.navbarLine}></hr>
        </div>
    );
};

export default Navbar;