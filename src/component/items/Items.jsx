import React, {useState} from 'react';
import classes from "./Items.module.css";
import {Link} from "react-router-dom";
import {useSelector} from "react-redux";
import ShowcaseItem from "../main/showcase/item/ShowcaseItem";
import MySelect from "./select/MySelect";

const Items = () => {
    const [selectedSort, setSelectedSort] = useState("")
    const items = useSelector(state => {
        switch (selectedSort) {
            case "name":
                return state.items.list.slice().sort((a, b) => a.name > b.name ? 1 : -1);
            case "price":
                return  state.items.list.slice().sort((a, b) => a.price > b.price ? 1 : -1);
            case "description":
                return  state.items.list.slice().sort((a, b) => a.description > b.description ? 1 : -1);
            case "brand":
                return  state.items.list.slice().sort((a, b) => a.brand > b.brand ? 1 : -1);
            default:
                return state.items.list
        }
    });

    function changeSort(sort) {
        setSelectedSort(sort)
    }

    return (
        <div className={classes.item}>
            <div className={classes.cookie}>
                <Link className={classes.cookieLink} to={`/`}>Главная</Link>
                <div>
                    <hr/>
                </div>
                <p className={classes.cookieLink}>Каталог</p>
            </div>
            <div className={classes.itemsBox}>
                <h2>Косметика и гигиена</h2>
                <div className={classes.sort}>
                    <p>Сортировка: </p>
                    <MySelect
                        value={selectedSort}
                        onChange={changeSort}
                        defaultValue=""
                        options={[
                            {value: 'name', name: 'Название'},
                            {value: 'price', name: 'Цена'},
                            {value: 'description', name: 'Описание'},
                            {value: 'brand', name: 'Бренд'}
                        ]}
                    />
                </div>
            </div>
            <div className={classes.itemsBox1}>
                <div className={classes.types}>
                    <p>Средства для стирки</p>
                </div>
                <div className={classes.types}>
                    <p>Моющие средства</p>
                </div>
                <div className={classes.types}>
                    <p>Уход для рук</p>
                </div>
                <div className={classes.types}>
                    <p>Уход для лица</p>
                </div>
            </div>
            <div className={classes.itemsBox2}>
                <div>
                    <p className={classes.params}>ПОДБОР ПО ПАРАМЕТРАМ</p>
                    <p>Цена ₸</p>
                    <div className={classes.price}>
                        <input></input>
                        <p>-</p>
                        <input></input>
                    </div>
                    <div className={classes.producer}>
                        <p>Производитель</p>
                        <div className={classes.search}>
                            <input type="text" placeholder="Поиск..."></input>
                            <button type="submit">
                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M15.5297 15.5294L12.0992 12.0928L15.5297 15.5294ZM14.0002 7.5C14.0002 9.22391 13.3154 10.8772 12.0964 12.0962C10.8775 13.3152 9.22415 14 7.50024 14C5.77634 14 4.12304 13.3152 2.90405 12.0962C1.68506 10.8772 1.00024 9.22391 1.00024 7.5C1.00024 5.77609 1.68506 4.12279 2.90405 2.90381C4.12304 1.68482 5.77634 1 7.50024 1C9.22415 1 10.8775 1.68482 12.0964 2.90381C13.3154 4.12279 14.0002 5.77609 14.0002 7.5V7.5Z"
                                        stroke="white" strokeWidth="1.3" strokeLinecap="round"/>
                                </svg>
                            </button>
                        </div>
                        <hr/>
                    </div>
                </div>
                <div className={classes.itemsBox3}>
                    {items.map(item =>
                        <ShowcaseItem item={item} key={item.id}/>
                    )}
                </div>
            </div>
        </div>
    );
};

export default Items;