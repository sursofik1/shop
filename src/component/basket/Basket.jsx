import React, {useState} from 'react';
import classes from "./Basket.module.css";
import {Link, useNavigate} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import BasketItem from "./BasketItem";
import {clearBasket} from "../../store/items.store";

const Basket = () => {
    const items = useSelector(state => {
        const items = []
        const basket = state.items.basket
        for (const basketItem of basket) {
            let item = state.items.list.find(item => item.id === basketItem.id);
            if (item !== undefined){
                items.push({...item, count: basketItem.count})
            }
        }
        return items
    });
    const sum = useSelector(state =>{
        const basket = state.items.basket;
        let sum = 0
        for (const basketItem of basket) {
            let item = state.items.list.find(item => item.id === basketItem.id);
            if (item !== undefined){
                sum += (basketItem.count * item.price)
            }
        }
        return sum
    });
    const dispatch = useDispatch();
    const navigate = useNavigate();

    function checkout(){
        dispatch(clearBasket())
        navigate("/order");
    }
    return (
        <div className={classes.basket}>
            <div className={classes.cookie}>
                <Link className={classes.cookieLink} to={`/`}>Главная</Link>
                <div>
                    <hr/>
                </div>
                <p className={classes.cookieLink}>Корзина</p>
            </div>
            <h2>Корзина</h2>
            <div className={classes.basketBox}>
                {items.map(item =>
                    <BasketItem item={item} key={item.id}/>
                )}
            </div>
            <hr/>
            <div className={classes.checkout}>
                <button onClick={checkout}>Оформить заказ</button>
                <p>{+sum.toFixed(2)} ₸</p>
            </div>
        </div>
    );
};

export default Basket;