import React from 'react';
import classes from "./Showcase.module.css";
import ShowcaseItem from "./item/ShowcaseItem";
import { useSelector, useDispatch } from 'react-redux';

const Showcase = (props) => {
    const items = useSelector(state => state.items.list.slice(0,8));
    return (
        <div className={classes.showcase}>
            <div className={classes.showcaseBox}>
                <h3 className={classes.text1}>Акционные</h3>
                <h3 className={classes.text2}>товары</h3>
            </div>
            <div className={classes.itemsBox}>
                {items.map(item =>
                        <ShowcaseItem item={item} key={item.id}/>
                )}
            </div>

        </div>
    );
};

export default Showcase;