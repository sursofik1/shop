import React from 'react';
import classes from "./Products.module.css";
import image1 from './images/1.png';
import image2 from './images/2.png';
import image3 from './images/3.png';
import image4 from './images/4.png';
import image5 from './images/5.png';
import image6 from './images/6.png';
import image7 from './images/7.png';
import image8 from './images/8.png';
import image9 from './images/9.png';
import image10 from './images/10.png';
import imageMap from './images/map.png';
import ProductsItem from "./ProductsItem";

const Products = () => {
    const products = [
        {image: image1},
        {image: image2},
        {image: image3},
        {image: image4},
        {image: image5},
        {image: image6},
        {image: image7},
        {image: image8},
        {image: image9},
        {image: image10},
    ]
    return (
        <div className={classes.products}>
            <div className={classes.categoryBox}>
                <h3 className={classes.text1}>Лучшие</h3>
                <h3 className={classes.text2}>товары</h3>
            </div>
            <p className={classes.text3}>От ведущих мировых брендов</p>
            <div className={classes.productsBox}>
                {products.map((product, idx) =>
                    <ProductsItem product={product} key={idx}/>
                )}
            </div>
            <div className={classes.mapItem}>
                <svg width="92" height="7" viewBox="0 0 92 7" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <rect width="26" height="7" rx="3.5" fill="#FFC85E"/>
                    <rect opacity="0.1" x="32" width="16" height="7" rx="3.5" fill="#3F4E65"/>
                    <rect opacity="0.1" x="54" width="16" height="7" rx="3.5" fill="#3F4E65"/>
                    <rect opacity="0.1" x="76" width="16" height="7" rx="3.5" fill="#3F4E65"/>
                </svg>
                <img src={imageMap}/>
            </div>
        </div>
    );
};

export default Products;