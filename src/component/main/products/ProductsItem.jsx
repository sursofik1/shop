import React from 'react';
import classes from "./ProductsItem.module.css";

const ProductsItem = (props) => {
    return (
        <div className={classes.itemBox}>
            <img src={props.product.image}/>
        </div>
    );
};

export default ProductsItem;