import React, {useState} from 'react';
import image1 from './images/1.png';
import image2 from './images/2.png';
import image3 from './images/3.png';
import image4 from './images/4.png';
import image5 from './images/5.png';
import CategoryItem from "./CategoryItem";
import classes from "./Category.module.css";
import image from "./images/carousel.png";

const Category = () => {
    const categories = [
        {image: image1, title: "Бытовая химия"},
        {image: image2, title: "Косметика и гигиена"},
        {image: image3, title: "Товары для дома"},
        {image: image4, title: "Товары для детей и мам"},
        {image: image5, title: "Посуда"},
    ]
    return (
        <div className={classes.category}>
            <div className={classes.categoryBox}>
                <h3 className={classes.text1}>Акционные</h3>
                <h3 className={classes.text2}>товары</h3>
            </div>
            <p className={classes.text3}>10 000+ ходовых позиций по спецмальным ценам</p>
            <div className={classes.itemsBox}>
                {categories.map((category, idx) =>
                    <CategoryItem category={category} key={idx}/>
                )}
            </div>
            <div className={classes.carousel}>
                <img src={image}/>
            </div>
        </div>
    );
};

export default Category;