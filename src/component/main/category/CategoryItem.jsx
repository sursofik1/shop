import React from 'react';
import classes from "./CategoryItem.module.css";
const CategoryItem = (props) => {
    return (
        <div className={classes.categoryBox}>
            <div className={classes.box}>
                <img src={props.category.image}/>
                <p>{props.category.title}</p>
            </div>
        </div>
    );
};

export default CategoryItem;