import React from 'react';
import classes from "./Picture.module.css";
import {Link} from "react-router-dom";

const Picture = () => {
    return (
        <div className={classes.picture}>
            <div className={classes.pictureBox}>
                <svg width="921" height="796" viewBox="0 0 921 796" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <g filter="url(#filter0_b_42_86)">
                        <rect width="921" height="796" fill="url(#paint0_linear_42_86)"/>
                    </g>
                    <defs>
                        <filter id="filter0_b_42_86" x="-13" y="-13" width="947" height="822"
                                filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                            <feFlood floodOpacity="0" result="BackgroundImageFix"/>
                            <feGaussianBlur in="BackgroundImageFix" stdDeviation="6.5"/>
                            <feComposite in2="SourceAlpha" operator="in" result="effect1_backgroundBlur_42_86"/>
                            <feBlend mode="normal" in="SourceGraphic" in2="effect1_backgroundBlur_42_86"
                                     result="shape"/>
                        </filter>
                        <linearGradient id="paint0_linear_42_86" x1="-1.2478e-05" y1="378.1" x2="921" y2="375.556"
                                        gradientUnits="userSpaceOnUse">
                            <stop stopColor="#8A94AB"/>
                            <stop offset="1" stopColor="#8A94AB" stopOpacity="0"/>
                        </linearGradient>
                    </defs>
                </svg>
                <div className={classes.textBox}>
                <h1>Бытовая химия,<br></br>
                    косметика<br></br>
                    и хозтовары</h1>
                <h2>оптом по кокчетаву и области</h2>
                    <Link to={'/items'} className={classes.buttonLink}>
                    <button className={classes.button}>
                        <p>В КАТАЛОГ</p>
                    </button>
                    </Link>
                    <div className={classes.pluseBox}>
                        <button className={classes.pluse}>+</button>
                        <p className={classes.pluseText}>Только самые<br></br>
                            выгодные предложения</p>
                        <button className={classes.pluse}>+</button>
                        <p className={classes.pluseText}>Бесплатная доставка<br></br>
                            по <b>Кокчетаву от 10 тыс ₸</b></p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Picture;