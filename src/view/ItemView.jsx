import React from 'react';
import Item from "../component/item/Item";
import Navbar from "../component/core/navbar/Navbar";
import Header from "../component/core/header/Header";
import Footer from "../component/core/footer/Footer";

const ItemView = () => {
    return (
        <div>
            <Navbar/>
            <Header/>
            <Item/>
            <Footer/>
        </div>
    );
};

export default ItemView;