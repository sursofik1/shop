import React from 'react';
import Navbar from "../component/core/navbar/Navbar";
import Header from "../component/core/header/Header";
import Order from "../component/order/Order";
import Footer from "../component/core/footer/Footer";
import Admin from "../component/admin/Admin";

const AdminView = () => {
    return (
        <div>
            <Navbar/>
            <Header/>
            <Admin/>
            <Footer/>
        </div>
    );
};

export default AdminView;