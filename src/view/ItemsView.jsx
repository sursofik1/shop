import React from 'react';
import Navbar from "../component/core/navbar/Navbar";
import Header from "../component/core/header/Header";
import Footer from "../component/core/footer/Footer";
import Items from "../component/items/Items";

const ItemsView = () => {
    return (
        <div>
            <Navbar/>
            <Header/>
            <Items/>
            <Footer/>
        </div>
    );
};

export default ItemsView;