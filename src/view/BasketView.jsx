import React from 'react';
import Navbar from "../component/core/navbar/Navbar";
import Header from "../component/core/header/Header";
import Footer from "../component/core/footer/Footer";
import Basket from "../component/basket/Basket";

const BasketView = () => {
    return (
        <div>
            <Navbar/>
            <Header/>
            <Basket/>
            <Footer/>
        </div>
    );
};

export default BasketView;