import React from 'react';
import Order from "../component/order/Order";
import Navbar from "../component/core/navbar/Navbar";
import Header from "../component/core/header/Header";
import Footer from "../component/core/footer/Footer";

const OrderView = () => {
    return (
        <div>
            <Navbar/>
            <Header/>
            <Order/>
            <Footer/>
        </div>
    );
};

export default OrderView;