import React from 'react';
import Picture from "../component/main/picture/Picture";
import Showcase from "../component/main/showcase/Showcase";
import Category from "../component/main/category/Category";
import Products from "../component/main/products/Products";
import Navbar from "../component/core/navbar/Navbar";
import Header from "../component/core/header/Header";
import {RouterProvider} from "react-router-dom";
import router from "../router";
import Footer from "../component/core/footer/Footer";

const HomeView = () => {
    return (
        <div>
            <Navbar/>
            <Header/>
            <Picture/>
            <Showcase/>
            <Category/>
            <Products/>
            <Footer/>
        </div>
    );
};

export default HomeView;