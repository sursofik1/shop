import './App.css';
import Navbar from "./component/core/navbar/Navbar";
import Header from "./component/core/header/Header";
import Footer from "./component/core/footer/Footer";
import {RouterProvider} from "react-router-dom";
import React from "react";
import router from "./router";


function App() {
    return (
        <div className="App">
            <Navbar/>
            <Header/>
            <RouterProvider router={router}/>
            <Footer/>
        </div>
    );
}

export default App;
