import {createSlice} from '@reduxjs/toolkit'

export const itemsSlice = createSlice({
    name: 'items',
    initialState: {
        list: require("./../data/data.json"),
        basket: []
    },
    reducers: {
        addItemToBasket: (state, action) => {
            const item = state.basket.find(item => item.id === action.payload.id);
            if (item !== undefined) {
                item.count += action.payload.count;
            } else {
                state.basket.push(action.payload);
            }
        },
        incrementCountBasket: (state, action) => {
            const id = action.payload;
            const item = state.basket.find(item => item.id === id);
            if (item !== undefined && item.count < 100) {
                item.count += 1
            }
        },
        decrementCountBasket: (state, action) => {
            const id = action.payload;
            const item = state.basket.find(item => item.id === id);
            if (item !== undefined && item.count > 0) {
                item.count -= 1;
            }
        },
        deleteItemInBasket: (state, action) => {
            const id = action.payload;
            state.basket = state.basket.filter(item => item.id !== id);
        },
        clearBasket: (state, action) =>{
            state.basket = [];
        },
        deleteItem: (state, action) =>{
            const id = action.payload;
            state.list = state.list.filter(item => item.id !== id);
        },
        editItem: (state, action) =>{
            const changedItem = action.payload;
            const item = state.list.find(item => item.id === changedItem.id);
            if (item !== undefined){
                item.name = changedItem.name;
                item.description = changedItem.description;
                item.size = changedItem.size;
                item.size_type = changedItem.size_type;
                item.barcode = changedItem.barcode;
                item.producer = changedItem.producer;
                item.brand = changedItem.brand;
                item.type = changedItem.type;
                item.price = changedItem.price;
            }
        }
    }
})
export const {
    addItemToBasket,
    incrementCountBasket,
    decrementCountBasket,
    deleteItemInBasket,
    clearBasket,
    deleteItem,
    editItem
} = itemsSlice.actions

export default itemsSlice.reducer