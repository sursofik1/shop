import { configureStore } from '@reduxjs/toolkit'
import itemsReducer from './items.store';
export default configureStore({
    reducer: {
        items: itemsReducer
    }
})